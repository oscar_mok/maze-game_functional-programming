import javax.swing.*;

/*
 * 資管4A
 * 105403031  莫智堯
 */

public class Main extends HW5_105403031{

    public static void main(String[] args){
        HW5_105403031 HW5_drawer = new HW5_105403031();
        HW5_drawer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        HW5_drawer.setSize(600, 750);
        HW5_drawer.setVisible(true);
    }
}
