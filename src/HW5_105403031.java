import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
 * 資管4A
 * 105403031  莫智堯
 */

public class HW5_105403031 extends JFrame {

    private final JProgressBar hp;
    private final JPanel main_JPanel;
    private List<Integer> list_int;
    private List<String> list;
    private ImageIcon heart;
    private ImageIcon diamond;
    private ImageIcon brickwall;

    public HW5_105403031(){
        super();

        //產生血條
        hp = new JProgressBar(0, 100); //血條範圍0 ~ 100
        hp.setValue(100); //血條起始值：100
        //

        main_JPanel = new JPanel();
        main_JPanel.setLayout(new GridLayout(10, 10)); //分割成100格

        //讀入png檔
        brickwall = new ImageIcon("src/brickwall.png");
        diamond = new ImageIcon("src/diamond.png");
        heart = new ImageIcon("src/heart.png");
        //

        //限制圖片大小
        brickwall.setImage(brickwall.getImage().getScaledInstance(60, 60, Image.SCALE_DEFAULT));
        diamond.setImage(diamond.getImage().getScaledInstance(60, 60, Image.SCALE_DEFAULT));
        heart.setImage(heart.getImage().getScaledInstance(60, 60, Image.SCALE_DEFAULT));
        //

        try {

            //把map.txt 檔轉成stream，再以空格作為分割，讓每個字都被獨立讀取到，轉傳作list 的data structure 型態
            list = Files.lines(Paths.get("src/map.txt")).map(s -> s.split(("\\s"))).flatMap(Arrays::stream).collect(Collectors.toList());

            //利用functional programming 內的方法，把list內的data 型態由String 轉為Integer
            list_int = list.stream().map(Integer::parseInt).collect(Collectors.toList());

            System.out.println(list_int);

            SecureRandom secureRandom = new SecureRandom();

            //建立10個 範圍由0 - 100 的隨機數，再用if 判斷 如果list 的隨機數位置value == 1（代表是牆壁）那個value 才改寫成5
            secureRandom.ints(10, 0, 100).forEach(ran -> {
                if (list_int.get(ran) == 1) list_int.set(ran, 5);
            });


            for (int i = 0; i < list_int.size(); i++){

                int temp = list_int.get(i);

                if (temp == 0){
                    //設定為通道
                    road();

                }else if (temp == 1){
                    //設定為牆
                    brickwall();

                }else if (temp == 5) {
                    //設定成愛心
                    heart();

                }else {
                    //設定為出口
                    diamond();

                }
            }


        } catch (IOException e) {
            System.err.println("Error while reading file.");
        }

        add(hp, BorderLayout.NORTH);
        add(main_JPanel, BorderLayout.CENTER);
    }

    private void diamond() {
        JLabel diamond_JLabel = new JLabel(diamond); //ImageIcon 不能直接add 進JPanel裡，所以要用JLabel包裝

        main_JPanel.add(diamond_JLabel);

        diamond_JLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if (hp.getValue() > 0) {
                    JOptionPane.showMessageDialog(null, "恭喜到達終點!!");
                }else {
                    JOptionPane.showMessageDialog(null, "到達終點，但血量已歸零 QQ");
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    private void brickwall(){

        JLabel brickwall_JLabel = new JLabel(brickwall); //把牆的圖片包到 JLabel裡

        main_JPanel.add(brickwall_JLabel); //每執行一次就把牆加到JPanel 的格子裡

        brickwall_JLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                hp.setValue(hp.getValue() -  20); //每碰到一次血條就減20hp
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    private void road(){
        JLabel road_JLabel = new JLabel(); //空的JLabel 道路不用imageicon

        main_JPanel.add(road_JLabel); //每執行一次就加到JPanel 的格子裡

        road_JLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                hp.setValue(hp.getValue() -  5); //每碰到一次血條就減5hp
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    private void heart() {

        JLabel heart_JLabel = new JLabel(heart); //把愛心的圖片包到 JLabel裡

        main_JPanel.add(heart_JLabel); //每執行一次就把愛心加到JPanel 的格子裡

        heart_JLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                hp.setValue(hp.getValue() +  15); //碰到愛心血條加15hp
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

}
